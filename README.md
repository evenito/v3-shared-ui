# README #

This is a shared code between evenito's UI projects.

### Local development ###
**IF you need** to add something into `shared-ui`, please don't, or at least think twice is it absolutely needed there.

If you're certain,
* pull this project to your local PC
* add volume `/shared-ui` to your **target** project's **docker-compose.private.yml** (look for example below)
* rebuild + rerun

```
---
version: '3.7'
services:
  ui:
    volumes:
      - ../shared-ui:/shared-ui
#      - ~/projects/vasiliy_pupkin/evenito/shared/ui:/shared_ui
    environment:
      VUE_APP_API_ROOT: https://api.dev3.evenito.com
#      VUE_APP_API_ROOT: http://api.evenito.local
```

Your local `shared-ui` be picked up by the build system.

### Configuration ###
All projects that use v3-shared-ui must set `inheritAttrs` to `false`