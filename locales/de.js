export default {
  "entities": {
    "contacts": {
      "statuses": {
        "declined": "Abgemeldet",
        "registered": "Angemeldet",
        "selected": "Ausstehend"
      },
      "translatableFieldTitles": {
        "companions": "Begleitungen",
        "companionSummary": "Zusammenfassung Begleitperson(en)",
        "connect_url": "Persönlicher evenito connect Link des Kontakts zum Einloggen",
        "files": "Dateien",
        "host": "Host",
        "qr": "QR-Code",
        "registrationSummary": "Registrierungs-Zusammenfassung ",
        "status": "Status",
        "tickets": "Ticket-Zusammenfassung",
        "token": "Bitte hier Token eingeben.",
        "totalParticipants": "Teilnehmer insgesamt"
      }
    },
    "datashapes": {
      "defaultKeyNiceName": {
        "email": "E-Mail",
        "firstName": "Vorname",
        "lastName": "Nachname"
      }
    },
    "events": {
      "translatableFieldTitles": {
        "addToCalendarGoogle": "Google Kalender-Link",
        "addToCalendarICS": "ICS-Link",
        "default_language": "Standard-Sprache",
        "description": "Beschreibung",
        "end_timestamp": "Ende",
        "languages": "Sprachen",
        "name": "Name",
        "register": "Persönlicher Anmelde-Link",
        "start_timestamp": "Start",
        "startDateWithDay": "Start-Datum mit Wochentag",
        "startTime": "Startzeit"
      }
    },
    "messageJobs": {
      "translatableFieldTitles": {
        "renderURL": "Link zum Anzeigen der Nachricht im Browser"
      }
    }
  },
  "placeholderSectionTitles": {
    "contactInformation": "Kontaktinformationen",
    "contactInviterInformation": "Informationen zum Einladenden",
    "eventInformation": "Eventinformationen",
    "files": "Dateien",
    "host": "Host",
    "links": "Links",
    "registrationInformation": "Anmeldeinformationen"
  },
  "shared": {
    "resources": {
      "eventType": {
        "food": "Food & Drinks",
        "networking": "Networking",
        "speech": "Vortrag",
        "workshop": "Workshop"
      },
      "languages": {
        "de": "Deutsch",
        "en": "Englisch",
        "es": "Spanisch",
        "fr": "Französisch",
        "it": "Italienisch",
        "nl": "Niederländisch (Beta)"
      }
    },
    "error": {
      "required": "Dies ist ein Pflichtfeld",
      "minNumber": "Diese Zahl muss größer oder gleich {amount} sein.",
      "maxNumber": "Diese muss kleiner oder gleich {amount} sein.",
      "notAnInteger": "Dies sollte eine Zahl ohne Dezimalstellen sein.",
      "notValidEmail": "Gib eine gültige E-Mail-Adresse ein",
      "notValidUrl": "Bitte verwende eine gültige URL"
    }
  }
}