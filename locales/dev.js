export default {
  shared: {
    resources: {
      eventType: {
        networking: 'Networking',
        food: 'Food',
        workshop: 'Workshop',
        speech: 'Speech',
      },
      languages: {
        en: 'English',
        de: 'German',
        fr: 'French',
        it: 'Italian',
        es: 'Spanish',
        nl: 'Dutch (beta)',
      },
    },
    error: {
      minNumber: 'This must be greater or equal to {amount}',
      maxNumber: 'This must be less or equal to {amount}',
      notAnInteger: 'This should be number without decimal',
      notValidEmail: 'This should be a valid e-mail',
      notValidUrl: 'This should be a valid URL',
      notValidPhoneNumber: 'This should be a valid phone number',
      required: 'This field is required',
    },
    label: {
      noDataAvailable: 'No data available',
    },
  },
};