export default {
  "entities": {
    "contacts": {
      "statuses": {
        "declined": "Declined",
        "registered": "Registered",
        "selected": "Pending"
      },
      "translatableFieldTitles": {
        "companions": "Companions",
        "companionSummary": "Companion(s) summary",
        "connect_url": "Contact's personal evenito connect link for log-in",
        "files": "Files",
        "host": "Host",
        "qr": "QR Code",
        "registrationSummary": "Registration summary",
        "status": "Status",
        "tickets": "Ticket summary",
        "token": "Please enter your personal token here.",
        "totalParticipants": "Total participants"
      }
    },
    "datashapes": {
      "defaultKeyNiceName": {
        "email": "E-mail",
        "firstName": "First name",
        "lastName": "Last name"
      }
    },
    "events": {
      "translatableFieldTitles": {
        "addToCalendarGoogle": "Google calendar link",
        "addToCalendarICS": "ICS link",
        "default_language": "Default language",
        "description": "Description",
        "end_timestamp": "End",
        "languages": "Languages",
        "name": "Name",
        "register": "Registration link",
        "start_timestamp": "Start",
        "startDateWithDay": "Start date with weekday",
        "startTime": "Start time"
      }
    },
    "messageJobs": {
      "translatableFieldTitles": {
        "renderURL": "Link to view message in browser"
      }
    }
  },
  "placeholderSectionTitles": {
    "contactInformation": "Contact information",
    "contactInviterInformation": "Inviter information",
    "eventInformation": "Event information",
    "files": "Files",
    "host": "Host",
    "links": "Links",
    "registrationInformation": "Registration information"
  },
  "shared": {
    "resources": {
      "eventType": {
        "food": "Food & Drinks",
        "networking": "Networking",
        "speech": "Speech",
        "workshop": "Workshop"
      },
      "languages": {
        "de": "German",
        "en": "English",
        "es": "Spanish",
        "fr": "French",
        "it": "Italian",
        "nl": "Dutch (beta)"
      }
    },
    "error": {
      "required": "This field is required",
      "minNumber": "This must be greater or equal to {amount}",
      "maxNumber": "This must be less or equal to {amount}",
      "notAnInteger": "This should be a number without decimals.",
      "notValidEmail": "E-mail must be valid",
      "notValidUrl": "Please use a valid URL"
    }
  }
}