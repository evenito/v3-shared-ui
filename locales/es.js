export default {
  "entities": {
    "contacts": {
      "statuses": {
        "declined": "Desconectado",
        "registered": "Conectado",
        "selected": "Pendiente"
      },
      "translatableFieldTitles": {
        "companions": "Accompagnamenti",
        "companionSummary": "Sintesi Accompagnatore/i",
        "connect_url": "Enlace personal evenito connect del contacto para iniciar sesión",
        "files": "Archivos",
        "host": "Host",
        "qr": "Código QR",
        "registrationSummary": "Resumen de la inscripción",
        "status": "Estado",
        "tickets": "Riassunto del biglietto",
        "token": "Por favor, introduce tu clave de registro",
        "totalParticipants": "Totale partecipanti"
      }
    },
    "datashapes": {
      "defaultKeyNiceName": {
        "email": "Posta elettronica",
        "firstName": "Nome",
        "lastName": "Apellido"
      }
    },
    "events": {
      "translatableFieldTitles": {
        "addToCalendarGoogle": "Collegamento a Google Calendar",
        "addToCalendarICS": "Collegamento ICS",
        "default_language": "Idioma por defecto",
        "description": "Descripción",
        "end_timestamp": "Finalizar",
        "languages": "Idiomas",
        "name": "Nombre",
        "register": "Link di accesso personale",
        "start_timestamp": "Inicio",
        "startDateWithDay": "Inicio con día de la semana",
        "startTime": "Hora de inicio"
      }
    },
    "messageJobs": {
      "translatableFieldTitles": {
        "renderURL": "Enlace para ver el mensaje en el navegador"
      }
    }
  },
  "placeholderSectionTitles": {
    "contactInformation": "Información de contacto",
    "contactInviterInformation": "Informazioni sull'invitante",
    "eventInformation": "Información sobre el evento",
    "files": "Archivos",
    "host": "Host",
    "links": "Enlaces",
    "registrationInformation": "Información sobre la inscripción"
  },
  "shared": {
    "resources": {
      "eventType": {
        "food": "Comida y bebida",
        "networking": "Red",
        "speech": "Conferencia",
        "workshop": "Taller"
      },
      "languages": {
        "de": "Alemán",
        "en": "Inglés",
        "es": "Español",
        "fr": "Francés",
        "it": "Italiano",
        "nl": "Holandés"
      }
    },
    "error": {
      "required": "Este es un campo obligatorio",
      "minNumber": "Este número debe ser mayor o igual a {amount}.",
      "maxNumber": "Debe ser inferior o igual a {importe}",
      "notAnInteger": "Debe ser un número sin decimales.",
      "notValidEmail": "Introduce una dirección de correo electrónico válida",
      "notValidUrl": "Por favor, utiliza una URL válida"
    }
  }
}