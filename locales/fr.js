export default {
  "entities": {
    "contacts": {
      "statuses": {
        "declined": "Non-souscrit",
        "registered": "Enregistré",
        "selected": "Enregistré"
      },
      "translatableFieldTitles": {
        "companions": "Accompagnements",
        "companionSummary": "Résumé Accompagnateur(s)",
        "connect_url": "Lien personnel evenito connect du contact pour la connexion.",
        "files": "Fichiers",
        "host": "Host",
        "qr": "Code QR",
        "registrationSummary": "Résumé de l'enregistrement",
        "status": "Statut",
        "tickets": "Synthèse de ticket",
        "token": "Veuillez entrer le token ici.",
        "totalParticipants": "Participants au total"
      }
    },
    "datashapes": {
      "defaultKeyNiceName": {
        "email": "Email",
        "firstName": "Prénom",
        "lastName": "Nom"
      }
    },
    "events": {
      "translatableFieldTitles": {
        "addToCalendarGoogle": "Lien Google Calendrier",
        "addToCalendarICS": "Lien ICS",
        "default_language": "Langue par défaut",
        "description": "Description",
        "end_timestamp": "Fin",
        "languages": "Langues",
        "name": "Nom",
        "register": "Lien d'inscription personnel",
        "start_timestamp": "Début",
        "startDateWithDay": "Date de début avec jour de la semaine",
        "startTime": "Heure de début"
      }
    },
    "messageJobs": {
      "translatableFieldTitles": {
        "renderURL": "Lien pour afficher le message dans le navigateur"
      }
    }
  },
  "placeholderSectionTitles": {
    "contactInformation": "Informations de contact",
    "contactInviterInformation": "Informations sur l'invitant",
    "eventInformation": "Informations sur l'événement",
    "files": "Fichiers",
    "host": "Host",
    "links": "Liens",
    "registrationInformation": "Informations sur l'inscription"
  },
  "shared": {
    "resources": {
      "eventType": {
        "food": "Alimentation",
        "networking": "Networking",
        "speech": "Séminaire",
        "workshop": "Workshop"
      },
      "languages": {
        "de": "Allemand",
        "en": "Anglais",
        "es": "Espagnol",
        "fr": "Français",
        "it": "Italien",
        "nl": "Néerlandais"
      }
    },
    "error": {
      "required": "Champ obligatoire",
      "minNumber": "Ce nombre doit être plus élevé ou être égal à {amount}.",
      "maxNumber": "Ce nombre doit être inférieur ou égal à {amount}.",
      "notAnInteger": "Il doit comporter un nombre sans décimales.",
      "notValidEmail": "Merci de saisir une adresse e-mail valide.",
      "notValidUrl": "Merci d’utiliser une URL valide"
    }
  }
}