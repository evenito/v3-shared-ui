import {reassemble} from '../utils/objectUtils.js';

import {default as dev} from './dev.js';
import {default as en} from './en.js';
import {default as de} from './de.js';
import {default as fr} from './fr.js';
import {default as it} from './it.js';
import {default as es} from './es.js';
import {default as nl} from './nl.js';

export const languages = {
  en: {
    value: 'en',
    subCode: 'en-US',
    aliases: ['en', 'eng'],
    strings: en,
  },
  fr: {
    value: 'fr',
    subCode: 'fr-FR',
    aliases: ['fr', 'fre', 'fra'],
    strings: fr,
  },
  de: {
    value: 'de',
    subCode: 'de-DE',
    aliases: ['de', 'deu', 'ger'],
    strings: de,
  },
  it: {
    value: 'it',
    subCode: 'it-IT',
    aliases: ['it', 'ita'],
    strings: it,
  },
  es: {
    value: 'es',
    subCode: 'es-ES',
    aliases: ['es', 'esp', 'spa', 'span'],
    strings: es,
  },
  nl: {
    value: 'nl',
    subCode: 'nl-NL',
    aliases: ['nl'],
    strings: nl,
  },
};

export const sharedLocales = {
  dev, en, de, fr, it, es, nl
};

export const getMessages = clientConfig => {
  return {
    ...reassemble(languages, (lang, declaration) => ({
      [lang]: {
        ...declaration.strings,
        ...clientConfig[lang]
      },
    })),
    dev: {
      ...dev,
      ...clientConfig['dev'],
    }
  };
}
