export default {
  "entities": {
    "contacts": {
      "statuses": {
        "declined": "Non iscritto",
        "registered": "Iscritto",
        "selected": "Iscritto"
      },
      "translatableFieldTitles": {
        "companions": "Accompagnatori",
        "companionSummary": "Riassunto dei compagni",
        "connect_url": "Link personale di evenito connect del contatto per il log-in",
        "files": "File",
        "host": "Host",
        "qr": "Codice QR",
        "registrationSummary": "Riepilogo della registrazione",
        "status": "Stato",
        "tickets": "Riepilogo biglietti",
        "token": "Si prega di inserire il token qui.",
        "totalParticipants": "Totale partecipanti"
      }
    },
    "datashapes": {
      "defaultKeyNiceName": {
        "email": "Email",
        "firstName": "Nome",
        "lastName": "Cognome"
      }
    },
    "events": {
      "translatableFieldTitles": {
        "addToCalendarGoogle": "Collegamento al calendario Google",
        "addToCalendarICS": "Collegamento ICS",
        "default_language": "Lingua predefinita",
        "description": "Descrizione",
        "end_timestamp": "Fine",
        "languages": "Lingue",
        "name": "Nome",
        "register": "Link di accesso personale",
        "start_timestamp": "Iniziare",
        "startDateWithDay": "Data di inizio con giorno",
        "startTime": "Ora di inizio"
      }
    },
    "messageJobs": {
      "translatableFieldTitles": {
        "renderURL": "Link per visualizzare il messaggio nel browser"
      }
    }
  },
  "placeholderSectionTitles": {
    "contactInformation": "Informazioni di contatto",
    "contactInviterInformation": "Informazioni sull'invitante",
    "eventInformation": "Informazioni sull'evento",
    "files": "File",
    "host": "Host",
    "links": "Link",
    "registrationInformation": "Informazioni per la registrazione"
  },
  "shared": {
    "resources": {
      "eventType": {
        "food": "Alimenti",
        "networking": "Networking",
        "speech": "Lecture",
        "workshop": "Workshop"
      },
      "languages": {
        "de": "Tedesco",
        "en": "Inglese",
        "es": "Spagnolo",
        "fr": "Francese",
        "it": "Italiano",
        "nl": "Olandese"
      }
    },
    "error": {
      "required": "Questo è un campo obbligatorio",
      "minNumber": "Questo numero deve essere superiore o uguale a {amount}.",
      "maxNumber": "Questo valore deve essere minore o uguale a {amount}.",
      "notAnInteger": "Questo numero non dovrebbe avere decimali.",
      "notValidEmail": "Inserisci un indirizzo email valido",
      "notValidUrl": "Utilizza un URL valido"
    }
  }
}