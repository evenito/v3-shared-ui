export default {
  "entities": {
    "contacts": {
      "statuses": {
        "declined": "Afgewezen",
        "registered": "Geregistreerd",
        "selected": "Openstaand"
      },
      "translatableFieldTitles": {
        "companions": "Metgezellen",
        "companionSummary": "Samenvatting metgezellen",
        "connect_url": "Contact's Connect url",
        "files": "Bestanden",
        "host": "Host",
        "qr": "QR Code",
        "registrationSummary": "Registratie samenvatting",
        "status": "Status",
        "tickets": "Ticket samenvatting",
        "token": "Token",
        "totalParticipants": "Totaal aantal deelnemers"
      }
    },
    "datashapes": {
      "defaultKeyNiceName": {
        "email": "E-mail",
        "firstName": "Voornaam",
        "lastName": "Achternaam"
      }
    },
    "events": {
      "translatableFieldTitles": {
        "addToCalendarGoogle": "Google calendar link",
        "addToCalendarICS": "ICS link",
        "default_language": "Standaard taal",
        "description": "Beschrijving",
        "end_timestamp": "Einde",
        "languages": "Talen",
        "name": "Naam",
        "register": "Registratielink",
        "start_timestamp": "Begin",
        "startDateWithDay": "Begindatum met dag",
        "startTime": "Start tijd"
      }
    },
    "messageJobs": {
      "translatableFieldTitles": {
        "renderURL": "Preview in browser url"
      }
    }
  },
  "placeholderSectionTitles": {
    "contactInformation": "Contact informatie",
    "contactInviterInformation": "Informatie over de uitnodiger",
    "eventInformation": "Evenement Informatie",
    "files": "Bestanden",
    "host": "Host",
    "links": "Links",
    "registrationInformation": "Registratie informatie"
  },
  "shared": {
    "resources": {
      "eventType": {
        "food": "Eten en drinken",
        "networking": "Netwerken",
        "speech": "Speech",
        "workshop": "Workshop"
      },
      "languages": {
        "de": "Duits",
        "en": "Engels",
        "es": "Spaans",
        "fr": "Frans",
        "it": "Italiaans",
        "nl": "Nederlands (beta)"
      }
    },
    "error": {}
  }
}