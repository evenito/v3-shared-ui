import { isString, isFunction, isPlainObject, difference } from 'lodash';
import { arrayToObject } from '../utils/objectUtils.js';
import { configurable } from '../utils/overrideUtils.js';

/***
const examples = {
  //no extra action, only close dialog, backoffice.common.button.cancel as a caption,
  cancel: '',

  //no extra action, SomeComponent.specialCancel as a caption,
  "SomeComponent.specialCancel": '',

  //link to some method, close dialog, backoffice.common.button.add as a caption,
  add: this.createNewPerson,

  //extra action in-lined, backoffice.common.button.saveChanges as a caption,
  saveChanges() {
    //perform save
  },

  // EventsSchedulesEdit.saveAndReturnText as a caption,
  "EventsSchedulesEdit.saveAndReturnText": () => {
    //perform save            //extra action in-lined
  },

  //handler() as extra action, text as is (not recommended)
  example1: {
    text: "Total "+self.translations.cancel, //never do it, please
    handler() {
      console.log("I'm doing something, and aborting everything");
      self.addNewFieldPage = false;
    }
  },

  //handler() as extra action, backoffice.event.location.label.addPhoto as caption
  example2: {
    label: "backoffice.event.location.label.addPhoto",
    handler() {
      //do something else
      console.log("I should leave the dialog open");
      return true;
    }
  },

  //icon, secondary, router.push
  'icon.add': '/events/new',

  //primary icon, action inlined
  'icon.primary.settings_outlined': () => {},

  // primary text, EventsSchedulesEdit.saveAndReturnText as a caption,
  'primary.EventsSchedulesEdit.saveAndReturnText': () => {
    //perform save
  },

  // secondary text, EventsSchedulesEdit.saveAndReturnText as a caption,
  'EventsSchedulesEdit.cancelAndReturnText': this.createNewPerson,
};
*/

const keyFlags = ['disabled', 'icon', 'primary', 'danger', 'outlined', 'accent', 'small'];

export default configurable({ defaultKey: undefined }, ({ defaultKey }) => ({
  methods: {
    lastIsPrimary(buttons) {
      if (buttons.length >= 1) {
        const button = buttons[buttons.length - 1];
        if (button.primary === undefined) button.primary = !button.danger;
      }
      return buttons;
    },
    unwrapButtons(object) {
      const toKey = key => !key ? undefined : (!defaultKey || key.includes('.') ? key : defaultKey + key);
      return Object.entries(object).map(([key, definition]) => {
        if (!definition && !isString(definition)) return undefined;
        let common = {};
        if (definition.component) {
          return { key, ...definition };
        } else if (key.includes('.')) {
          let parts = key.split('.');
          const enabledKeyFlags = arrayToObject(keyFlags, flag => ({ [flag]: parts.includes(flag) || undefined }));
          const isIcon = enabledKeyFlags.icon;
          const remainder = difference(parts, keyFlags).join('.');
          common = {
            key,
            ...enabledKeyFlags,
            icon: isIcon ? remainder : undefined,
            label: isIcon ? undefined : toKey(remainder),
          };
        } else { //key has no dots
          try {
            common = {
              key,
              // when object has text and icon undefined, fetch translation using test in label or key
              // if label is nest (ie. its string includes '.') use label instead of key
              label: !definition.icon ? toKey(definition.label || key) : undefined,
            };
          } catch (e) {
            if (!('' + e).includes('Translation key')) console.warn(e);
          }
        }

        if (isString(definition)) {
          return definition ? {
            ...common,
            to: definition, //non-empty string - router url
          } : {
            ...common,
            onClick: () => {}, //empty string - no action
          };
        } else if (isFunction(definition)) {
          return {
            ...common,
            onClick: (...args) => { return definition(...args) },
          };
        } else if (isPlainObject(definition)) {
          const isRoute = definition.name && definition.params;
          return isRoute ? {
            ...common,
            to: definition,
          } : {
            ...common,
            key,
            ...(isFunction(definition.handler) ? { onClick: (...args) => { return definition.handler(...args) } } : {}),
            ...definition, //rest of definition passed as is
            ...(definition.appendIcon ? { icon: definition.appendIcon } : {}),
            // ...(definition.label) ? {label: definition.label ? this.$t(definition.label) : definition.text} : {},
          };
        } else throw "I don't know how to handle this button definition: " + key;
      }).filter(v => v);
    },
  },
}));
