export default (event, handler) => {
  let bound = undefined;
  return {
    mounted() {
      bound = handler.bind(this);
      this.$el.addEventListener(event, bound);
    },
    beforeUnmount() {
      if (bound) this.$el.removeEventListener(event, bound);
    },
  };
};