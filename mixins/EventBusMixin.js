import mitt from 'mitt';
const bus = mitt();

const cache = {};

const post = (event, data = null, sticky = false) => {
  if (sticky) cache[event] = data;
  bus.emit(event, data);
};

const subscribe = (event, callback) => {
  bus.on(event, callback);
  if (cache[event]) {
    callback(cache[event], true); // const callback = (data, isCached) => {}
  }
  return () => bus.off(event, callback);
};

// Please do not export more than the post method, we export limited functionality for reason.
// If you need to use subscriptions rather use mixin which is safe since it also unsubscribes automatically.
export const EventBus = { post };

export default {
  data: () => ({
    subscriptionCleanupCallbacks: [],
  }),
  computed: {
    eventBus() {
      const _self = this;
      return {
        subscribe: (event, callback) => {
          _self.subscriptionCleanupCallbacks.push(subscribe(event, callback));
        },
        post,
      };
    },
  },
  unmounted() {
    this.subscriptionCleanupCallbacks.forEach(cb => cb());
    this.subscriptionCleanupCallbacks = [];
  },
};
