import { isFunction, isString } from 'lodash';
import { atom } from 'nanostores';
import { isReactive, reactive } from 'vue';
import { setDeep } from '../utils/deepUtils.js';
import generateHex from '../utils/generateHex.js';
import { reassemble } from '../utils/objectUtils.js';

export default stores => {
  const dynamic = {};

  return {
    useStore: stores,
    computed: {
      ...reassemble(stores, (storeName, store) => ({
        [storeName]() {
          const dynamicId = store.factory && store.args && this.dynamicId[storeName];
          return dynamicId ? this.storeCache.state[storeName][dynamicId] : this.storeCache.state[storeName];
        },
      })),
    },
    beforeCreate() {
      if (!this.dynamicId) this.dynamicId = {};
      if (!this.storeCache?.state) setDeep(this, 'storeCache.state', reactive({}));
      reassemble(stores, (storeName, store) => {
        if (store.factory && store.args) { // dynamic store handling
          dynamic[storeName] = store;
          this.dynamicId[storeName] = this.dynamicId[storeName] || generateHex();
          if (!this.storeCache?.state?.[storeName]) setDeep(this, `storeCache.state.${storeName}`, reactive({}));
          setDeep(this, `storeCache.state.${storeName}.${this.dynamicId[storeName]}`, undefined);
        } else {
          if (isFunction(this?.storeCache?.unsubscribe?.[storeName])) { // if there's a previous subscriber, unsubscribe it
            this?.storeCache?.unsubscribe?.[storeName]();
          }
          setDeep(this, `storeCache.unsubscribe.${storeName}`, store.subscribe(value => {
            setDeep(this, `storeCache.state.${storeName}`, isReactive(value) ? value : reactive(value));
          }));
        }
      });
    },
    beforeMount() {
      const vm = this;
      reassemble(dynamic, (storeName, store) => {
        const getValue = arg => isString(arg) ? vm[arg] : isFunction(arg) ? arg(vm) : arg;
        const atoms = vm?.storeCache?.[storeName]?.atoms || store.args.map(arg => atom(getValue(arg)));
        if (!vm?.storeCache?.[storeName]?.atoms) setDeep(vm, `storeCache.${storeName}.atoms`, atoms);
        const dynId = vm.dynamicId[storeName];
        if (!vm?.storeCache[storeName]?.constructed) { // if no store was constructed before, construct it and save it
          setDeep(vm, `storeCache.${storeName}.constructed`, store.factory(...atoms));
        }
        if (isFunction(vm?.storeCache?.unsubscribe?.[storeName])) { // if there's a previous subscriber, unsubscribe it
          vm?.storeCache?.unsubscribe?.[storeName]();
        }
        setDeep( // subscribe a new listener to a constructed store
          vm, `storeCache.unsubscribe.${storeName}`, 
          vm.storeCache[storeName].constructed.subscribe(value => {
            setDeep(vm, `storeCache.state.${storeName}.${dynId}`, value);
          }),
        );
        if (!vm.storeCache?.stopWatch) vm.storeCache.stopWatch = [];
        vm.storeCache.stopWatch.push(vm.$watch(
          () => store.args.map(arg => getValue(arg)),
          newArgs => newArgs.forEach((v, idx) => atoms[idx].set(v)),
          { immediate: true, deep: true },
        ));
      });
    },
    beforeUnmount() {
      this.storeCache.stopWatch?.forEach(stop => stop());
      this.storeCache.stopWatch = [];
      const { unsubscribe } = this.storeCache;
      reassemble(unsubscribe, (storeName, handler) => {
        handler();
        delete unsubscribe[storeName];
      });
    },
  };
};