import { camelCase } from 'lodash';
import { isSame } from '../utils/objectUtils.js';

function generate(required = true, propName = 'modelValue', eventName = 'update:modelValue', addon = '') {
  return {
    emits: [eventName],
    props: {
      [propName]: {
        required,
      },
    },
    computed: {
      [camelCase(`vModel-${addon}`)]: {
        get: function() {
          return this[propName];
        },
        set: function(newValue) {
          this.$emit(eventName, newValue);
        },
      },
    },
  };
}

function generateData(required = true, propName = 'modelValue', eventName = 'update:modelValue', addon = '') {
  const vModelName = camelCase(`vModel-${addon}`);
  return {
    emits: [eventName],
    props: {
      [propName]: {
        required,
      },
    },
    data: () => ({
      [vModelName]: undefined,
    }),
    watch: {
      [propName]: {
        immediate: true,
        handler(value) {
          if (isSame(this[vModelName], value)) return;
          this[vModelName] = value;
        },
      },
      [vModelName]: {
        handler(value) {
          if (isSame(this[propName], value)) return;
          this.$emit(eventName, value);
        },
      },
    },
  };
}


export const VModelMixin = generate();
export const VModelMixinOptional = generate(false);
export default VModelMixin;

export const VModelAddonMixin = prop => generate(true, prop, `update:${prop}`, prop);
export const VModelAddonMixinOptional = prop => generate(false, prop, `update:${prop}`, prop);

export const VModelDataMixin = generateData();
export const VModelDataMixinOptional = generateData(false);

export const VModelDataAddonMixin = prop => generateData(true, prop, `update:${prop}`, prop);
export const VModelDataAddonMixinOptional = prop => generateData(false, prop, `update:${prop}`, prop);