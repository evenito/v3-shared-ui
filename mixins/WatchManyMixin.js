import { get, isFunction } from 'lodash';

export default {
  watchMany: {
    /*
     semanticName_ignored: {
       keys: ['key1', 'nested.key2', 'key3'],
       handler(key1, nested_key2, key3) {
         this.whatever = key1 ? nested_key2 : (key3 * key1);
       },
       immediate: true /!*FORCED*!/,
       deep: false,
     },
    */
  },
  created() {
    const watched = Object.keys(this.$options.watchMany);
    watched.forEach(name => {
      const _declaration = this.$options.watchMany[name];
      const valid = isFunction(get(_declaration, 'handler')) && get(_declaration, 'keys.length');
      if (!valid) return;
      const handler = key => {
        if (_declaration.debug) {
          // eslint-disable-next-line no-console
          console.log(`WatchMany: ${name} was triggered by ${key}`);
        }
        const params = _declaration.keys.map(key => get(this, key));
        get(_declaration, 'handler').bind(this)(...params);
      };
      _declaration.keys.forEach(key => this.$watch(key, ignored => handler(key), _declaration.deep ? { deep: true } : {}));
      handler();
    });
  },
};
