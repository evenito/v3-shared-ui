import { atom, onMount } from 'nanostores';
import { resolveAndNotify, storeToValue } from './storeUtils.js';

/**
 * WARNING, this store is asynchronous only.
 * It takes around 2 ticks to update the value for .get()
 * Use .subscribe to get the value eventually
 * */
export const deepComputed = (stores, compute, debug) => {
  if (!Array.isArray(stores)) stores = [stores];
  const $computed = atom(undefined);

  let previousArgs;
  const set = () => {
    const args = stores.map($store => $store.get());
    if (
      previousArgs === undefined ||
      args.some((arg, i) => storeToValue(arg) !== previousArgs[i])
    ) {
      previousArgs = args.map(storeToValue);
      resolveAndNotify($computed, compute(...args));
    }
  };

  let timer;
  const run = () => {
    clearTimeout(timer);
    timer = setTimeout(set);
  };

  onMount($computed, () => {
    const unbinds = stores.map($store => $store.listen(run));
    set();
    return () => unbinds.forEach(unbind => unbind());
  });

  return $computed;
};

