import { atom, onMount } from 'nanostores';
import { looksLikeStore, resolveAndNotify, storeToValue } from './storeUtils.js';

export const dynamic = (storesGetter, compute) => {
  const $dynamic = atom(undefined);
  $dynamic.dynamic = true;

  let previousArgs;
  let lastStores = [];
  let batched = undefined;
  let timer;

  const resetDynamic = () => {
    $dynamic.set(undefined);
    lastStores = [];
    previousArgs = undefined;
  };

  const set = () => {
    if (!batched) batched = () => {
      clearTimeout(timer);
      timer = setTimeout(set);
    };

    const stores = storesGetter().filter(looksLikeStore);
    lastStores // unsubscribe from obsolete
      .filter(item => !stores.includes(item.store))
      .forEach(item => item.unsubscribe());
    if (!stores.length) {
      resetDynamic();
      return;
    }
    lastStores = stores
      .map(item => {
        const existing = lastStores.find(i => i.store === item);
        if (existing) {
          return existing;
        } else { // new item, subscribe
          return { store: item, unsubscribe: item.listen(batched) };
        }
      });

    const args = stores.map($store => $store.get());
    if (
      previousArgs === undefined ||
      args.some((arg, i) => storeToValue(arg) !== previousArgs[i])
    ) {
      previousArgs = args.map(storeToValue);
      resolveAndNotify($dynamic, compute(...args));
    }
  };

  onMount($dynamic, () => {
    set();
    return () => {
      lastStores
        .map(item => item.unsubscribe)
        .forEach(unbind => unbind());
      resetDynamic();
    };
  });

  return $dynamic;
};
