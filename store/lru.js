import { isPlainObject } from 'lodash';
import { map } from 'nanostores';
import { comparators } from '../utils/comparators.js';
import { override } from '../utils/overrideUtils.js';

export const lru = size => {
  const $lru = map({});
  const timestamps = {};

  const trim = () => {
    const sorted = Object.entries(timestamps)
      .map(([key, timestamp]) => ({ key, timestamp }))
      .sort(comparators.extract(i => i.timestamp, comparators.date));
    sorted.slice(0, Math.max(sorted.length - size, 0))
      .forEach(({ key }) => $lru.deleteKey(key));
  };


  override($lru, {
    deleteKey(key) {
      this.super.setKey(key, undefined);
    },
    setKey(key, value) {
      if (value !== undefined) {
        timestamps[key] = Date.now();
        this.super.setKey(key, value);
        trim();
      }
      return value;
    },
    set(values) {
      if (!isPlainObject(values)) {
        throw new Error('lru.set expects object as a parameter');
      }
      Object.keys(values)
        .forEach(key => timestamps[key] = Date.now());
      this.super.set(values);
      trim();
      return $lru;
    },
  });

  $lru.getKey = key => {
    const value = $lru.value[key];
    if (value) timestamps[key] = Date.now();
    return value;
  };

  return $lru;
};
