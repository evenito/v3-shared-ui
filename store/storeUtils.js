import { isFunction, isPlainObject } from 'lodash';
import { reassemble } from '../utils/objectUtils.js';

export const constructState = (...parts) => {
  let state = {};
  for (const part of parts) {
    if (isFunction(part)) {
      state = { ...state, ...part(state) };
    } else if (isPlainObject(part)) {
      state = { ...state, ...part };
    }
  }
  return state;
};

export const looksLikeStore = value => value &&
  isPlainObject(value) &&
  isFunction(value.get) &&
  isFunction(value.listen);

export const storeToValue = storeOrValue => looksLikeStore(storeOrValue)
  ? storeOrValue.get()
  : storeOrValue;

/** WARNING,
 * this will update the state on a next tick,
 * so reading store state in the same tik will return old value
 * */
export const resolveAndNotify = (store, value) => {
  Promise.resolve(value)
    .then(value => {
      if (value !== store.value) {
        store.set(value);
      } else {
        store.notify(value);
      }
    });
};

export const subscribeTo = stores => {
  const state = {
    unsubscribe: {},
  };
  reassemble(stores, (storeName, store) => {
    const stateKey = storeName.startsWith('$') ? storeName.slice(1) : storeName;
    state.unsubscribe[storeName] = store.subscribe(value => {
      state[stateKey] = value;
    });
  });
  return state;
};
