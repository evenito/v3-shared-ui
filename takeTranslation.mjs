/*
*   USAGE:
*   yarn node takeTranslation.mjs common.key1 r/common.key2 platform-ui.search.key1:renderer.target.key2
*
*   so params are the list of keys to transfer
*   - if key specified as whatever.key.you.need.to.transfer (without colon or slash)
*     it will be transferred with the same key to shared-ui (this project) from platform-ui
*
*   - if key specified as platform-ui.search.key1:renderer.target.key2
*     it will be transferred with the new key
*
*   - if source key has r/ in front, it will be transferred from website renderer
* */


import fs from 'fs';
import { get, set } from 'lodash';
import * as platformUiLocales from '../platform-ui/src/locales/index.js';
import * as rendererLocales from '../website-renderer/client/src/locales/index.js';
import { sharedLocales } from './locales/index.js';

const keyPairs = process.argv.slice(2);
 
keyPairs.forEach(keyPair => {
  const [sourceKey, targetKey] = keyPair.split(':');
  const target = targetKey || sourceKey;
  const sourceLocales = sourceKey.startsWith('r/') ? rendererLocales : platformUiLocales;
  const source = sourceKey.startsWith('r/') ? sourceKey.slice(2) : sourceKey;
  console.log('----------------------------------');
  Object.entries(sharedLocales)
    .forEach(([lang, locale]) => {
      const value = get(sourceLocales[lang], source);
      set(locale, target, value);
      console.log(`${lang} | ${target} = ${value}`);
    });
});

Object.entries(sharedLocales)
  .forEach(([lang, locale]) => {
    let stringify = JSON.stringify(locale, null, 2);
    if (lang === 'dev') {
      stringify = stringify
        .replace(/"([a-zA-Z]+)":\s*"([^'\n]+)",?/g, "$1: '$2',")    // key: 'single quotes',
        .replace(/"([a-zA-Z]+)":\s*"([^"\n]+)",?/g, '$1: "$2",')  // key: " value's double quotes",
        .replace(/"([a-zA-Z]+)":/g, '$1:')                          // key: {
        .replace(/},?\n/g, '},\n')                                  // },
        .replace(/}$/g, '};')                                       // }; //EOF
      ;
    }
    fs.writeFileSync(`./locales/${lang}.js`, `export default ${stringify}`);
  });
