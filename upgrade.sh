#!/bin/bash

NODE=false
SHARED=false
VAP=false
PLATFORM_UI=false

if [[ $1 == "" ]]; then
  NODE=true
  SHARED=true
  VAP=true
  PLATFORM_UI=true
else
  while getopts nsvp flag
  do
    case "${flag}" in
        n) NODE=true;;
        s) SHARED=true;;
        v) VAP=true;;
        p) PLATFORM_UI=true;;
        *) ;;
    esac
  done
fi


install_latest_yarn() {
  echo "== Installing latest yarn =="
  yarn set version latest
  yarn plugin import interactive-tools
}

install_minor_version_packages() {
  echo "== Installing latest minor packages =="
  npx npm-check-updates --target minor --upgrade
  yarn
  yarn upgrade-interactive
}

git_pull() {
  git checkout -B master origin/master && \
  git pull
}

git_push() {
  git add . && \
  echo -n "Enter to continue..."
  read input
  git commit -m "Libraries upgrade" && \
  git push
}

if $NODE; then
  echo "== Installing latest node =="
  NVM_DIR="$HOME/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
  nvm install node
  nvm use node
fi

cd .. # Get out of shared-ui directory

if $SHARED; then
  echo "== Upgrading shared-ui =="
  (cd ./v3-shared-ui && \
    git_pull && \
    install_latest_yarn && \
    install_minor_version_packages && \
    yarn build && \
    git_push
  )
fi

if $VAP; then
  echo "== Upgrading vap =="
  (cd ./vap-frontend && \
    git_pull && \
    install_latest_yarn && \
    yarn up:shared-ui && \
    install_minor_version_packages && \
    yarn build && \
    git_push
  )
fi

if $PLATFORM_UI; then
  echo "== Upgrading platform-ui =="
  (cd ./platform-ui && \
    git_pull && \
    install_latest_yarn && \
    yarn up:shared-ui && \
    install_minor_version_packages && \
    yarn build && \
    git_push
  )
fi