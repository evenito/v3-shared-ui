const knownAssetBaseUrls = [
  'https://evenito-v3-dev-files.web.app',
  'https://fs.evenito.com',
  'https://a.staging.evenito.dev',
];

export const directoryMime = 'evenito/directory';
export const imageMimes = ['image/jpeg', 'image/png', 'image/webp'];

export const assetUtils = {
  isAssetUrl: url => knownAssetBaseUrls.some(baseUrl => url?.startsWith(baseUrl)),
  isDirectory: file => file?.content_type === directoryMime,
};