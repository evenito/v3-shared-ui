import { camelCase, kebabCase } from 'lodash';
import { arrayToObject } from './objectUtils.js';

export default {
  listeners(...events) {
    return arrayToObject(events, event => {
      const name = camelCase(`on-${event}`);
      const handler = this.$attrs[name];
      return handler && { [name]: handler };
    });
  },
  attrs(...attrs) {
    return arrayToObject(attrs, name => {
      const kebabCaseName = kebabCase(name);
      return [kebabCaseName, camelCase(name)]
        .map(key => {
          const value = this.$attrs[key];
          return value !== undefined && { [kebabCaseName]: value }; // Strict check undefined because in template of <component primary />, $attrs will be {primary: ""}
        })
        .find(v => v);
    });
  },
};