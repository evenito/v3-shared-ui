import { isArray, isEmpty, isFinite, isFunction, isObject, mergeWith } from 'lodash';

export const falsyOrEmpty = a => {
  if (!a && !isFinite(a)) return true;
  if (isArray(a)) return isEmpty(a) || a.reduce((res, val) => res && falsyOrEmpty(val), true);
  if (isObject(a)) return isEmpty(a) || Object.keys(a).reduce((res, key) => res && falsyOrEmpty(a[key]), true);
  return false;
};

// WARNING: ! This is Vue 3 only version.
// Using it in Vue 2 will result in loss of data reactivity.
export const setDeep = (target, path, value) => {
  if (!path.includes('.')) {
    target[path] = value;
  } else {
    try {
      const chunks = path.split('.');
      const kid = chunks[0];
      const remainder = chunks.slice(1).join('.');
      if (!isObject(target[kid])) target[kid] = {};
      setDeep(target[kid], remainder, value);
    } catch (e) {
      console.warn('setDeep error', e);
    }
  }
};

export const mergeDeep = (...objects) => {
  const last = objects[objects.length - 1];
  let mods = [];
  let sources = objects;
  if (isArray(last?.mods) && Object.keys(last).length === 1) {
    mods = last.mods;
    sources = objects.slice(0, -1);
  }
  mods = [
    ...mods,
    (a, b) => {
      if (isArray(a) && isArray(b)) {
        return b;
      } else if (isFunction(a) && isFunction(b)) {
        return b;
      }
    },
  ];
  return mergeWith({}, ...sources, (a, b) => mods.reduce(
    (result, mod) => (result === undefined) ? mod(a, b) : result,
    undefined),
  );
};
mergeDeep.mods = {
  isVueComponent: (a, b) => {
    if ([a, b].every(v => v && isFunction(v.render) && isArray(v.staticRenderFns))) return b;
  },
  mergeArrays: (a, b) => {
    if (isArray(a) && isArray(b)) return [...a, ...b];
  },
};
