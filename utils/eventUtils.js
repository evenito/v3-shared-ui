import { isFunction } from 'lodash';

const keyCodeMap = {
  32: 'space',
  27: 'esc',
  38: 'up',
  40: 'down',
  13: 'enter',
};

export const onKey = (handlers, onKeyExtend) => e => {
  const handler = handlers[keyCodeMap[e.keyCode]];
  if (isFunction(handler)) {
    handler(e);
  } else if (isFunction(onKeyExtend)) {
    onKeyExtend(e);
  }
};