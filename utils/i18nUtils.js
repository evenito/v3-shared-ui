import { omit, uniq, get } from 'lodash';
import { languages } from '../locales/index.js';
import { falsyOrEmpty } from './deepUtils.js';
import { reassemble } from './objectUtils.js';

export const to18n = valueGenerator => ({
  i18n: true,
  ...reassemble(
    languages,
    lang => ({ [lang]: { value: valueGenerator(lang) }}),
  ),
});

export const $all_v = value => to18n(lang => (value || ''));

export const $fill_empty = (v, defaultLanguage) => {
  const filler = v[defaultLanguage]?.value || Object.values(v).find(l => l !== true && l.value)?.value;
  return to18n(lang => v?.[lang]?.value || filler || '');
};

export const $isEmpty_i18n = v => falsyOrEmpty(Object.values(omit(v, 'i18n')).map(v => v.value));

export const anyTranslationMissing = (languages, obj, allowEmptyObj = false) => {
  if (!languages.length) {
    console.warn('Empty list of languages passed', (new Error).stack);
    return false;
  }
  if ($isEmpty_i18n(obj)) {
    return !allowEmptyObj;
  }
  return uniq(languages.map(lng => !get(obj, `${lng}.value`, false))).length > 1;
};