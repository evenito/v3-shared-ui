import { isEmpty, omit, pick, isPlainObject, isString } from 'lodash';
import { assetUtils } from './assetUtils.js';
import { reassemble } from './objectUtils.js';

/*
* imageUtils.js fully supports image transformation for
* - Object with id, ex. {id: 'image-id'}
* - Asset string urls (read more in `assetUtils.js`)
*
* For string urls that are not "asset" string urls, it will return the original url without any image transformation.
* */

const dimensionFields = ['w', 'h'];
const isSupportedString = image => isString(image) && assetUtils.isAssetUrl(image);
const isSupportedImage = image => (isPlainObject(image) && Object.keys(image).includes('id')) || isSupportedString(image);

const fitMapper = {
  cover: 'outside',
};

export default baseAssetUrl => {
  const self = {
    getImageUrl: image => { // returns image url without any transformation.
      // Usually used by client who have image id but doesn't know base asset url
      if (!image) return '';
      if (!isSupportedImage(image) || isSupportedString(image)) return image;
      const imageId = image.id;
      return imageId ? `${baseAssetUrl}/${imageId}` : '';
    },
    transformImage: (image, transformations) => { // returns transformed image url
      if (!image) return '';
      if (!isSupportedImage(image)) return image;
      const url = self.getImageUrl(image);
      if (!url) return '';
      const query = Object.entries({
        q: 80,
        dpr: 1,
        ...transformations,
        fit: fitMapper[transformations.fit],
      }).filter(([k, v]) => v)
        .map(([k, v]) => `${k}=${v}`)
        .join('&');
      return `${url}?${query}`;
    },
    transformImageSet: (image, transformations) => { // returns transformed image set in various density
      // in format of {'1x': '1x-url', '2x': '2x-url', ...}
      if (!image || !transformations) return {};
      if (!isSupportedImage(image)) return { '1x': image };
      const nonDimensionalFields = omit(transformations, dimensionFields);
      const dimensionalFields = pick(transformations, dimensionFields);
      return [1, 1.5, 2, 3].reduce((acc, multiplier) => {
        const adjustedDimensions = reassemble(dimensionalFields, (key, value) => {
          const adjusted = value * multiplier * 1.2;
          return { [key]: adjusted > 8192 ? 8192 : adjusted };
        });
        const url = self.transformImage(image, { ...adjustedDimensions, ...nonDimensionalFields });
        return { ...acc, [`${multiplier}x`]: url };
      }, {});
    },
    getImageSet: (image, transformations) => { // returns transformed image set
      // in format of {'default': 'default-url', 'set': 'image-set-url'}
      // which can be used in `src` and `srcset` of img attributes
      if (!image) return undefined;
      if (!isSupportedImage(image)) return { default: image, set: '' };
      const imageSet = self.transformImageSet(image, transformations);
      if (isEmpty(imageSet)) return { default: self.getImageUrl(image), set: '' };
      return {
        default: imageSet['1x'],
        set: Object.entries(imageSet)
          .map(([multiplier, url]) => `${url} ${multiplier}`)
          .join(','),
      };
    },
    getBackgroundSetCss: (image, transformations) => { // returns transformed image set
      // in format of css string
      // which can be used to set background image
      if (!image) return undefined;
      if (!isSupportedImage(image)) return `background-image: url('${image}');`;
      const imageSet = self.transformImageSet(image, transformations);
      if (isEmpty(imageSet)) return `background-image: url('${self.getImageUrl(image)}');`;
      const backgroundSet = Object.entries(imageSet)
        .map(([multiplier, url]) => `url('${url}') ${multiplier}`)
        .join(', ');
      return ['image-set', '-webkit-image-set'] // Put real property last after chrome bug is fixed https://bugs.chromium.org/p/chromium/issues/detail?id=630597#c49
        .map(method => `background-image: ${method}(${backgroundSet});`)
        .join(' ');
    },
  };
  return self;
};