export const intersectionObserver = (handler, target, options = { threshold: [1] }) => {
  const observer = (typeof IntersectionObserver === 'function')
    ? new IntersectionObserver(
      ([{ target, intersectionRatio: ratio }]) => handler(target, ratio),
      options,
    )
    : {
      // sorry, IE https://caniuse.com/#feat=intersectionobserver
      observe: el => console.warn('IntersectionObserver is not supported, skipping'),
    };
  if (target) observer.observe(target);
  return observer;
};
