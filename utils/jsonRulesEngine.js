import { Engine } from 'json-rules-engine';

export const getJsonRulesEngine = () => new Engine();