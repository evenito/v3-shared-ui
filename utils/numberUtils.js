export const numberOrNull = v => v === 0 || v ? Number(v) : null;
export const numberAsStringOrNull = v => (v === 0 || v) && isFinite(Number(v)) ? String(v) : null;

export const isNullOrUndefined = v => v === null || v === undefined;

export const constrain = (v, min, max) => Math.min(Math.max(v, min), max);

export const defaultIfNullOrUndefined = (v, defaultValue) => isNullOrUndefined(v) ? defaultValue : v;

export const isNumeric = v => !isNaN(parseFloat(v)) && isFinite(v);