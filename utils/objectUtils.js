import { isArray, isEqual, isPlainObject, isString, range } from 'lodash';

export const arrayToObject = (
  array = [],
  transformer = (key => ({ [key]: key })),
) => Object.assign({}, ...array.map(transformer));

export const reassemble = (object = {}, transformer = (key, value) => ({ [key]: value }) ) => Object.assign({},
  ...Object.entries(object)
    .map(([key, value]) => transformer(key, value)) //return undefined to filter out
    .filter(e => !!e),
);

export const isSame = (a, b) => {
  const extract = value => {
    if (isPlainObject(value)) return { ...value };
    if (isArray(value)) return [...value];
    return value;
  };
  return isEqual(extract(a), extract(b));
};

/**
 * Potentially expensive operation.
 * If you have other options - consider them first.
 * If you have an ID of the object - use it.
 * If you're dealing with large objects - don't use this function
 * If you're iterating through massive amounts of items - don't use this.
 *
 * On the other side - 700 objects of small size (7 props + 1 array of 6 props objects)
 * was hashed individually in 22ms.
 * @param what - Object or String
 * @returns {string} stable hash
 */
export const hash = what => {
  const str = isString(what) ? what : JSON.stringify(what);
  return range(str.length)
    .map(i => str.charCodeAt(i))
    .reduce((hash, code) => {
      const newHash = (hash << 5) - hash + code;
      return newHash & newHash;
    }, 0)
    .toString(16);
};

export const evIf = (...parts) => Object.assign({}, ...parts.filter(v => v && isPlainObject(v)));