import { comparators } from './comparators.js';
import { maxBy } from 'lodash';

export const orderComparator = comparators.extract(item => item.order_num || 0, comparators.number);
export const vapOrderComparator = comparators.extract(item => item.orderNum || 0, comparators.number);
export const createdAtComparator = comparators.extract(item => item.created_at || item.createdAt || '', comparators.string);

export const setOrder = (item, order) => item.order_num = order;

export const orderMapper = (item, idx) => ({ ...item, order_num: idx });

export const nextOrder = list => {
  const max = maxBy(list, 'order_num');
  if (!max) return 1;
  return (max.order_num || 0) + 1;
};
