import { isArray, isFunction, isPlainObject, isString, omit, uniq } from 'lodash';
import { setDeep } from './deepUtils.js';
import { evIf, reassemble } from './objectUtils.js';

const debugOut = false;

const debugLog = (...what) => { if (debugOut) console.log(...what); };

export const getOverride = (target, overrides) => {
  const value = {};
  const superCollector = reassemble(overrides, (methodName, method) => {
    const originalMethod = target?.[methodName];
    if (isFunction(method)) {
      value[methodName] = function(...args) {
        const self = this;
        return method.bind(new Proxy(self, {
          get: (target, prop) => prop === 'super'
            ? reassemble(superCollector, (methodName, method) => ({ [methodName]: method.bind(self) }))
            : self[prop],
        }))(...args);
      };
    }
    return isFunction(originalMethod) && { [methodName]: originalMethod };
  });
  return value;
};


export const override = (target, overrides) => {
  Object.assign(target, getOverride(target, overrides));
  return target;
};

const computedOverride = (target, overrides) => {
  const value = {};
  const superCollector = reassemble(overrides, (methodName, method) => {
    const originalMethod = target?.[methodName];
    //TODO: this implementation does not cover {get(), set(v)} case
    //we need to implement it
    if (isFunction(method)) {
      value[methodName] = function(...args) {
        const self = this;
        return method.bind(new Proxy(self, {
          get: (target, prop) => prop === 'super'
            ? new Proxy(superCollector, { get: (target, prop) => target?.[prop]?.bind(self)() })
            : self[prop],
        }))(...args);
      };
    }
    return isFunction(originalMethod) && { [methodName]: originalMethod };
  });
  return value;
};

const collectFromParents = (current, what) => {
  if (!current) return {};
  return {
    ...collectFromParents(current.extends, what),
    ...current[what],
  };
};

const nodeType = {
  renderFn: 'renderFn',
  array: 'array',
  symbol: 'symbol',
  html: 'html',
  component: 'component',
  unknown: 'unknown',
};

const nodeTypes = [
  node => isFunction(node)                                  && nodeType.renderFn,
  node => isArray(node)                                     && nodeType.array,
  node => typeof node.type === 'symbol'                     && nodeType.symbol, //v-if, v-for, multiple-node-root, slot consumer
  node => isString(node.type)                               && nodeType.html,
  node => isPlainObject(node.type)                          && nodeType.component,
  node =>                                                      nodeType.unknown,
];
const getNodeType = node => node && nodeTypes.reduce((found, test) => found || test(node), undefined);

const nodeDescriptors = {
  [nodeType.renderFn]: node => { debugger /* should never happen */ },
  [nodeType.array]: arr => arr.length === 1 ? describeNode(arr[0]) : arr.map(i => describeNode(i)),
  // slotConsumer: node => ({ slot: node.key.slice(1) }),
  [nodeType.symbol]: node => ({ symbol: Symbol.keyFor(node.type), children: node.children?.map?.(i => describeNode(i)) || node.children }),
  [nodeType.html]: node => ({
    tag: node.type,
    class: node.props?.class,
    props: node.props,
  }),
  [nodeType.component]: node => ({
    component: node.type,
    class: node.props?.class,
    props: node.props,
  }),
  unknown: node => {
    debugger;
    return { unknown: node.type };
  },
};

const describeNode = node => nodeDescriptors[getNodeType(node)](node);

const visitHtmlTags = node => {
  const type = getNodeType(node);
  if (type === nodeType.array) {
    // handle array
    node.forEach(n => visitHtmlTags(n, scope));
  } else if (type === nodeType.symbol) {
    // handle other symbol. Does nothing, process only children
  } else if (type === nodeType.html) {
    // handle html tag
    let scopes = node.ctx?.type?.additionalScopes || [];
    const contextType = node.ctx?.vnode?.ctx?.vnode?.type;
    const scopeId = node.ctx?.vnode?.scopeId;
    if (contextType.additionalScopes?.length && (scopeId === contextType?.__scopeId || contextType.additionalScopes.includes(scopeId))) {
      //TODO: contextType.additionalScopes doesn't include additional scope that context type extends, probably we will need to add it
      scopes = uniq([...scopes, ...contextType.additionalScopes]);
    }
    if (!scopes?.length) return;
    debugLog('...applied scopes', describeNode(node), scopes.join(', '));
    scopes.forEach(scope => node.props ? node.props[scope] = true : node.props = { [scope]: true });
  }


  if (isArray(node.children) && node.children?.length) {
    node.children
      .filter(kid => kid.ctx?.vnode === node.ctx.vnode)
      .forEach(child => visitHtmlTags(child));
  }
};

const setAdditionalScopes = (type, srcType = type) => {
  let scopes = [];
  let current = srcType;
  while (current) {
    if (
      current?.__scopeId &&
      current?.extends?.__scopeId &&
      (current?.__scopeId !== current?.extends?.__scopeId)
    ) {
      scopes = [...scopes, current.extends.__scopeId];
    }
    current = current.extends;
  }
  if (scopes.length) {
    type.additionalScopes = uniq([...(type.additionalScopes || []), ...scopes]);
    debugLog(` ... rendered type ${type.__scopeId} has additional scopes ${type.additionalScopes.join(', ')}`);
  }
};

const wrapRender = (renderFunction, scope, { classAddon } = {}) => {
  if (renderFunction) {
    if (renderFunction.overridden !== (scope || true)) {
      debugLog(`   wrapping ${renderFunction.overridden ? 'scoped (' + renderFunction.overridden + ')' : 'native'} render function`, scope);
      const newRenderFunction = function render(...args) {
        let rendered = renderFunction.bind(this)(...args);
        debugLog('>>> Rendered', describeNode(rendered), { node: rendered }, this);

        if (classAddon && [nodeType.html, nodeType.component].includes(getNodeType(rendered))) {
          setDeep(
            rendered,
            'props.class',
            uniq([...(rendered.props?.class || '').split(/\s+/), classAddon]
              .map(s => s.trim())
              .filter(v => v),
            ).join(' '),
          );
        }

        const handleSingularRenderedTypes = node => {
          if (node.scopeId === null) {
            const missedScope = node.ctx?.vnode?.type?.__scopeId || node.ctx?.vnode?.type?.extends?.__scopeId;
            if (missedScope) {
              node.scopeId = missedScope;
              if (!node.ctx?.vnode?.type?.__scopeId) {
                debugLog(`... missing scope ${missedScope} propagated from base component`);
                node.ctx.vnode.type.__scopeId = node.ctx?.vnode?.type?.extends?.__scopeId;
              } else {
                debugLog(`... cannot propagate missing scope ${node.ctx?.vnode?.type?.extends?.__scopeId} since ${missedScope} already there`);
              }
            }
          }

          const type = getNodeType(node);
          if (type === nodeType.symbol) {
            setAdditionalScopes(node.ctx.type);
          } else if (type === nodeType.component) {
            setAdditionalScopes(node.type);
            setAdditionalScopes(node.type, node.ctx.type);
          } else if (type === nodeType.html) {
            setAdditionalScopes(node.ctx.type);
            if (node.ctx?.type?.additionalScopes?.length) {
              visitHtmlTags(node);
            }
          }
        };

        if (getNodeType(rendered) === nodeType.array) {
          rendered.forEach(node => handleSingularRenderedTypes(node));
        } else handleSingularRenderedTypes(rendered);

        return rendered;
      };
      newRenderFunction.overridden = (scope || true);
      return newRenderFunction;
    } else if (renderFunction.overridden === (scope || true)) {
      return renderFunction;
    }
  } else {
    debugger;
  }
};

const wrapSetupOrRender = (smth, scope, { classAddon } = {}) => {
  if (typeof smth === 'symbol') return {};
  const setupFunction = smth?.setup;
  const renderFunction = smth?.render;
  if (setupFunction) {
    if (setupFunction.overridden !== (scope || true)) {
      const setup = (props, context) => {
        const returnValue = setupFunction(props, context);
        return isFunction(returnValue) ? wrapRender(returnValue, scope, { classAddon }) : returnValue;
      };
      setup.overridden = (scope || true);
      return { setup };
    } else {
      return {};
    }
  } else if (renderFunction) {
    const wrapped = wrapRender(renderFunction, scope, { classAddon });
    return renderFunction === wrapped ? {} : { render: wrapped };
  } else {
    return {};
  }
};

export const extend = (_super, self = {}) => {
  const scope = _super.__scopeId;
  return {
    ...wrapSetupOrRender(_super, scope, { classAddon: self.class }),
    extends: _super,
    ...omit(self, ['computed', 'methods']),
    computed: computedOverride(collectFromParents(_super, 'computed'), self.computed),
    methods: getOverride(collectFromParents(_super, 'methods'), self.methods),
    ...evIf(scope && { additionalScopes: [scope] }), //this is a shortcut. Proper fix would be to visit already rendered HTML with newly propagated scope in render function
  };
};

export const configurable = (config, original) => ({
  ...original(config),
  configure(configOverride) {
    const compiled = this;
    const reconfigured = original({ ...config, ...configOverride });
    const extended = extend({ ...compiled, ...reconfigured }, omit(configOverride, Object.keys(config)));
    const { __file, __scopeId } = compiled; // this is probably redundant because of evif in `extend` method
    return {
      ...extended,
      __file,
      __scopeId,
    };
  },
});
