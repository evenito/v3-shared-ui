import { isArray, isBoolean, isFinite, isString } from 'lodash';
import isEmail from 'validator/es/lib/isEmail';
import isInt from 'validator/es/lib/isInt';
import isURL from 'validator/es/lib/isURL';

export const rules = $t => {
  const rule = {
    required: {
      validator: v => isFinite(v) ||
        isBoolean(v) && v ||
        isString(v) && v.trim().length > 0 ||
        isArray(v) && v.length > 0 ||
        $t('shared.error.required'),
      attrs: {
        required: true,
        'aria-required': true,
      },
    },
    email: v => !!v && v
      .split(',')
      .map(mail => isEmail(mail.trim()))
      .every(Boolean) || $t('shared.error.notValidEmail'),
    url: v => !!v && isURL(v) || $t('shared.error.notValidUrl'),
    minNumber: amount => v => v >= amount || $t('shared.error.minNumber', { amount }),
    maxNumber: amount => v => amount === undefined || v <= amount || $t('shared.error.maxNumber', { amount }),
    positiveNumber: v => rule.minNumber(0)(v),
    integer: v => isInt(String(v)) || $t('shared.error.notAnInteger'),
  };
  return rule;
};
