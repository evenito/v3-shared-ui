import { isNumeric } from './numberUtils.js';

export const numberToPixel = size => isNumeric(size) ? `${size}px` : size;