import { getJsonRulesEngine } from './jsonRulesEngine.js';

let jsonRulesScript = undefined;

export const initializeJsonRulesScript = async rootApi => {
  if (!jsonRulesScript) jsonRulesScript = await import(/* @vite-ignore */`${rootApi}/static/jsonrules.js`);
  return jsonRulesScript;
};

export const getExtendedJsonEngine = () => {
  if (!jsonRulesScript) return;
  const { default: extendJsonSchema } = jsonRulesScript;
  const engine = getJsonRulesEngine();
  return extendJsonSchema(engine);
};

export const getDatashapeEngine = datashape => {
  if (!jsonRulesScript) return;
  const { getDatashapeEngine: scriptGetDatashapeEngine } = jsonRulesScript;
  const engine = getExtendedJsonEngine();
  return scriptGetDatashapeEngine(engine, datashape);
};