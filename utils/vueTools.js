import { isFunction } from 'lodash';
import { reassemble } from './objectUtils.js';

const extractName = v => v && v.split('/').slice(-1)[0];

export const whoAmI = vueComponent => extractName(vueComponent.$options.__file);

export const findParent = (child, test, dontSearchDeeper) => {
  let current = child;
  const { localDev } = child;
  while (current.$parent) {
    if (test(current)) {
      if (localDev && current.$parent && !dontSearchDeeper && findParent(current.$parent, test, true)) {
        throw `${whoAmI(child)} searched for parent that is not unique`;
      }
      return current;
    }
    current = current.$parent;
  }
  return undefined;
};

export const findParentProvider = (child, providerName) => {
  if (window.localDev && !providerName.endsWith('Provide')) throw `${whoAmI(child)} tries to access non-provided data of a parent`;
  const parent = findParent(child, c => c[providerName]);
  return parent ? parent[providerName] : undefined;
};

const scrollRegex = /(scroll|auto)/;
const scroll = node => {
  let style;
  try {
    style = window.getComputedStyle(node, null);
  } catch (e) {
    debugger;
  }
  return scrollRegex.test(style.getPropertyValue('overflow'));
};
export const findScrollParent = node => {
  return !node || (node === document.documentElement) || (node === document) ? document.documentElement : scroll(node) ? node : findScrollParent(node.parentNode);
};

export const findParentBySelector = (element, selector) => {
  while (element && element !== document) {
    if (element.matches(selector)) return element;
    element = element.parentNode;
  }
  return null;
};

export const reduceHierarchy = (source, valueExtractor = v => undefined) => {
  /* WARNING, this doesn't merge computed and methods
   * if you use it with v => v value extractor it will not give you full vue structure
   * use more specific value extractors
   * for example v => v?.computed will give you all computed props of the hierarchy */
  const selfValue = valueExtractor(source);
  const sources = [ ...(source.mixins || []), source.extends].filter(v => v);
  if (!sources?.length) return selfValue;
  return sources
    .map(src => reduceHierarchy(src, valueExtractor))
    .reduce((a, b) => ({ ...b, ...a }), selfValue || {});
};

export const findInHierarchy = (source, test) => {
  const selfValue = test(source);
  if (selfValue) return selfValue;
  const sources = [ ...(source.mixins || []), source.extends].filter(v => v);
  if (!sources?.length) return undefined;
  return sources
    .reduce((found, src) => found || findInHierarchy(src, test), undefined);
};

export const override = overrides => {
  return reassemble(overrides, (name, fn) => {
    let _override = function(...args) {
      const self = this;
      if (!fn.super) {
        fn.super = findInHierarchy(this.$options, v => {
          let found = v?.computed?.[name];
          if (found && found !== _override) {
            fn.computed = true;
            return found;
          }
          found = v?.methods?.[name];
          if (found && found !== _override) {
            fn.computed = false;
            return found;
          }
          return undefined;
        });
      }
      return fn.bind(new Proxy(self, {
        get: (target, prop) => {
          if (prop === 'super' && isFunction(fn.super)) {
            const boundSuper = fn.super.bind(self);
            return { [name]: fn.computed ? boundSuper() : boundSuper };
          } else {
            return self[prop];
          }
        },
      }))(...args);
    };
    return { [name]: _override };
  });
};